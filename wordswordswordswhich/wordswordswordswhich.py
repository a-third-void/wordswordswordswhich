import math
import os

class WordsWordsWordsWhich:

    def latlon2words(self, lat, lon):
        return self.ijk2words(*self.m2ijk(self.n2m(self.XYxy2n(*self.latlon2XYxy(lat, lon)))))

    def words2latlon(self, words):
        return self.XYxy2latlon(*self.n2XYxy(self.m2n(self.ijk2m(*self.words2ijk(words)))))

    def latlon2XYxy(self, lat, lon):
        lon24 = (lon+180) * 24
        lat24 = (lat+90) * 24
        X = math.floor(lon24)
        Y = math.floor(lat24)
        return (
            X,
            Y,
            math.floor(self.W(Y) * self.frac(lon24)),
            math.floor(1546 * self.frac(lat24)),
        )

    def XYxy2latlon(self, X, Y, x, y):
        lat = (Y + (y+0.5)/1546) / 24 - 90
        lon = (X + (x+0.5)/self.W(Y)) / 24 - 180
        return (
            math.floor(lat * 1000000) / 1000000,
            math.floor(lon * 1000000) / 1000000,
        )

    def XYxy2n(self, X, Y, x, y):
        return self.XY2q(X, Y) + x * 1546 + y

    def n2XYxy(self, n):
        X, Y, q = self.n2XYq(n)
        n -= q
        x = math.floor(n / 1546)
        y = n - x * 1546

        return (X, Y, x, y)

    def m2ijk(self, m):
        l = math.floor(self.cuberoot(m))
        l2 = l * l
        l3 = l2 * l
        if l3 <= m and m < l3 + l2 + 2*l + 1:
            r = m-l3
            i = l
            j = r / (l+1)
            k = r % (l+1)
        elif l3 + l2 + 2*l + 1 <= m and m < l3 + 2*l2 + 3*l + 1:
            r = m - (l3 + l2 + 2*l + 1)
            i = r / (l+1)
            j = l
            k = r % (l+1)
        else:
            r = m - (l3 + 2*l2 + 3*l + 1)
            i = r / l
            j = r % l
            k = l

        return (math.floor(i), math.floor(j), math.floor(k))


    def m2ijk(self, m):
        l = math.floor(self.cuberoot(m))
        l2 = l * l
        l3 = l2 * l
        if l3 <= m and m < l3 + l2 + 2*l + 1:
            r = m-l3
            i = l
            j = r / (l+1)
            k = r % (l+1)
        elif l3 + l2 + 2*l + 1 <= m and m < l3 + 2*l2 + 3*l + 1:
            r = m - (l3 + l2 + 2*l + 1)
            i = r / (l+1)
            j = l
            k = r % (l+1)
        else:
            r = m - (l3 + 2*l2 + 3*l + 1)
            i = r / l
            j = r % l
            k = l
        return (math.floor(i), math.floor(j), math.floor(k))


    @staticmethod
    def ijk2m(i, j, k):
        l = max(i, j, k)
        l2 = l * l
        l3 = l2 * l
        if i == l:
            return l3 + (l+1)*j + k
        elif j == l:
            return l3 + l2 + 2*l + 1 + (l+1)*i + k
        else:
            return l3 + 2*l2 + 3*l + 1 + l*i + j

    def ijk2words(self, i, j, k):
        return ".".join([self.num2word[i], self.num2word[j], self.num2word[k]])

    def words2ijk(self, words):
        return [self.word2num[word] for word in words.split(".")]

    def n2m(self, n):
        for i in range(len(self.shuffle_blocks)):
            blockstart = self.shuffle_blocks[i][0]
            blocksize = self.shuffle_blocks[i][1]
            F_i = self.shuffle_blocks[i][2]

            if n >= blockstart and n < blockstart+blocksize:
                return ((n - blockstart) * F_i % blocksize) + blockstart

        raise ValueError("Can't n2m n=%d" % (n))

    def m2n(self, m):
        for i in range(len(self.shuffle_blocks)):
            blockstart = WordsWordsWordsWhich.shuffle_blocks[i][0]
            blocksize = WordsWordsWordsWhich.shuffle_blocks[i][1]
            R_i = WordsWordsWordsWhich.shuffle_blocks[i][3]

            if m >= blockstart and m < blockstart + blocksize:
                return ((m - blockstart) * R_i % blocksize) + blockstart
        raise ValueError("Can't m2n m=%d" % (m))

    def W(self, Y):
        return max(1, math.floor(1546 * math.sin(self.deg2rad((Y + 0.5) / 24))))

    @staticmethod
    def deg2rad(deg):
        return deg * math.pi / 180

    @staticmethod
    def cuberoot(n):
        return n ** (1. / 3)

    @staticmethod
    def frac(n):
        return n - math.floor(n)

    def XY2q(self, X, Y):
        min_i = 0
        max_i = len(self.regions_y[Y])

        while (min_i < max_i):
            i = math.floor((min_i + max_i) / 2)

            x1, y, length, q = self.regions_y[Y][i]
            if x1 <= X and x1 + length > X and y == Y:
                return q + (X - x1) * self.W(Y) * 1546
            elif X < x1:
                max_i = i
            else:
                min_i = i + 1

        raise ValueError("no such cell (%d,%d)" % (X,Y))

    def n2XYq(self, n):
        min_i = 0
        max_i = len(self.regions)

        while (min_i < max_i):
            i = math.floor((min_i + max_i) / 2)

            x1, y, length, startq = self.regions[i]

            onecell_size = self.W(y) * 1546
            endq = startq + length * onecell_size

            if n >= startq and n < endq:
                i = math.floor((n - startq) / onecell_size)
                return (x1 + i, y, startq + i * onecell_size)
            elif n < startq:
                max_i = i
            else:
                min_i = i + 1

        raise ValueError("no such cell (n=%d)" % (n))


    def build_regions(self):
        self.regions = []
        self.regions_y = {i: [] for i in range(4320)}

        q = 0
        r = self.region_data

        for i in range(math.ceil(len(r) / 6)):
            x = r[i * 6] + 256 * r[i * 6 + 1]
            y = r[i * 6 + 2] + 256 * r[i * 6 + 3]
            length = r[i * 6 + 4] + 256 * r[i * 6 + 5]
            region = (x, y, length, q)
            self.regions.append(region)
            self.regions_y[y].append(region)
            q += length * self.W(y) * 1546

        for i in range(4320):
            self.regions_y[i].sort()

    def build_words(self):
        self.num2word = [w for w in self.word_data.split("\n") if w]
        self.word2num = {self.num2word[i]: i for i in range(len(self.num2word))}

    def __init__(self):
        with open(os.path.join(os.path.dirname(__file__), "region_data.bin"), "rb") as f:
            self.region_data = f.read()
        with open(os.path.join(os.path.dirname(__file__), "word_data.txt")) as f:
            self.word_data = f.read()
        self.build_regions()
        self.build_words()

    shuffle_blocks = [
        [0, 15625000000, 9401181441, 12459868161],
        [15625000000, 109375000000, 87526181441, 12459868161],
        [125000000000, 296875000000, 118776181441, 184334868161],
        [421875000000, 578125000000, 525026181441, 418709868161],
        [1000000000000, 953125000000, 337526181441, 840584868161],
        [1953125000000, 1421875000000, 1400026181441, 668709868161],
        [3375000000000, 1984375000000, 1790651181441, 59334868161],
        [5359375000000, 2640625000000, 2618776181441, 59334868161],
        [8000000000000, 3390625000000, 2275026181441, 778084868161],
        [11390625000000, 4234375000000, 228151181441, 1934334868161],
        [15625000000000, 5171875000000, 1228151181441, 3934334868161],
        [20796875000000, 6203125000000, 2673590513073, 1560207133137],
        [27000000000000, 7328125000000, 2275026181441, 1434334868161],
        [34328125000000, 8546875000000, 7962526181441, 1949959868161],
        [42875000000000, 9859375000000, 1587526181441, 403084868161],
        [52734375000000, 11265625000000, 11025026181441, 7778084868161],
    ]


if __name__ == "__main__":
    wfw = WordsWordsWordsWhich()
    words = wfw.latlon2words(37.234332, -115.806657)  # == "joyful.nail.harmonica"
    print("(37.234332, -115.806657) -> ", words)
    latlon = wfw.words2latlon("joyful.nail.harmonica") # == [37.234328,-115.806657]
    print("joyful.nail.harmonica    -> ", latlon)
