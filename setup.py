from setuptools import setup, find_packages

setup(
    name="wordswordswordswhich",
    version=0.1,
    packages=find_packages(),
    include_package_data=True,
    install_requires=[],
    setup_requires=[],
    data_files = [("", ["wordswordswordswhich/word_data.txt", "wordswordswordswhich/region_data.bin"])],
    ext_modules=[],
)
